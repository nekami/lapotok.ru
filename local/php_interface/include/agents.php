<?
function DeleteFileFtpServer()
{	
	// текущая дата\время	
	$date = new DateTime();

	// директория на сайте для картинок
	$mkdir = $_SERVER['DOCUMENT_ROOT']."/upload/img/";
	
	// файлы директории
	$files = scandir($mkdir, 1);
	
	foreach ($files as $file)
	{
		if ( ($file != "..") && ($file != ".") )
		{
			$timeAddFile = date("Y-m-d H:i:s", filemtime($mkdir.$file));
			
			$diff = $date->diff( new DateTime($timeAddFile) )->format("%d");
			
			if ($diff > 14)
			{
				unlink($mkdir.$file);
			}
		}
	}

	return "DeleteFileFtpServer();";
}


function AddFileFtpServer()
{
	/* ДАНО */

	// текущая дата\время	
	$date = date("Y-m-d H:i:s");

	// идентификатор соединения	
	$conn_id = OpenConnectFtp($date);

	// директория на сайте для картинок
	$mkdir = $_SERVER['DOCUMENT_ROOT']."/upload/img/"; 

	// список файлов с удаленного сервера
	$files = ftp_nlist($conn_id, ".");

	// сбор массива всех имен файлов
	$arNameFtpFile = [];
	
	if ($conn_id)
	{
		/* СЧИТЫВАНИЕ ФАЙЛОВ КОРНЕВОЙ ДИРЕКТОРИИ */
		/* И ДЕЛЕНИЕ ИХ НА ПОРЦИИИ ДЛЯ ОБРАБОТКИ В РАМКАХ ОДНОГО FTP-СОЕДИНЕНИЯ */

		$circle = 1;
		$j = 0;

		for ($i = 0; $i < count($files); $i++)  
		{
			if ( strpos($files[$i], ".jpg") || strpos($files[$i], ".gif") || strpos($files[$i], ".png") ) 
			{	
				++$j;
				
				$name_file = explode("./", $files[$i])[1];			
				
				$arNameFtpFile[$circle][$name_file] = $files[$i];
				
				if ($j >= 1000) { $j = 0; ++$circle; }
			}	
		}
		
		// закрытие соединения
		ftp_close($conn_id); 
	}	

	/* КОПИРОВАНИЕ ФАЙЛОВ */

	foreach ($arNameFtpFile as $key => $Item)
	{
		$conn_id = OpenConnectFtp($date);
		
		if ($conn_id)
		{
			foreach ($Item as $LocalFile => $ServerFile) 
			{
				// размер локального файла
				$SizeLocalFile = filesize($mkdir.$LocalFile);
				
				// размер удаленного файла					
				$SizeServerFile = ftp_size($conn_id, $ServerFile);			
				
				if ( !file_exists($mkdir.$LocalFile) || ($SizeLocalFile != $SizeServerFile) )
				{	
					try {					
						if ( !ftp_get($conn_id, $mkdir.$LocalFile, $ServerFile, FTP_BINARY ) ) 
						{									
							throw new Exception("$date ошибка копирования файла $ServerFile");
						} 
						else
						{
							throw new Exception("$date успешное копирование файла $ServerFile");
						}							
					}	
							
					catch (Exception $e) 
					{
						p($e->getMessage(), $_SERVER['DOCUMENT_ROOT']."/logs/write_files.txt");
					}
				}
			}
			
			ftp_close($conn_id);
		}
	}	
   
    return "AddFileFtpServer();";
}

function OpenConnectFtp(&$date)
{
	// установка ftp-соединения

	$ftp_server    = \COption::GetOptionString( "askaron.settings", "UF_FTP_SERVER" );
	$ftp_user_name = \COption::GetOptionString( "askaron.settings", "UF_FTP_USER" );						
	$ftp_user_pass = \COption::GetOptionString( "askaron.settings", "UF_FTP_PASS" );		
						
	// установка соединения
	$conn_id = ftp_connect($ftp_server); 

	// вход с именем пользователя и паролем
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 
	
	// включение пассивного режима
	ftp_pasv($conn_id, true);

	// проверка соединения
	if ((!$conn_id) || (!$login_result)) 
	{		
		p("$date Не удалось установить соединение с FTP сервером! Попытка подключения к серверу $ftp_server под именем $ftp_user_name!", $_SERVER['DOCUMENT_ROOT']."/logs/write_files.txt");
		exit; 
	} 
	
	return $conn_id;
}
?>