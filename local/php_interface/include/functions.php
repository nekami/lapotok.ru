<?php 
    function s($var, $userID = 0)
    {
        global $USER;
        
        if (($userID > 0 && $USER->GetID() == $userID) || $userID == 0) {
            if ($var === false) {
                $var = "false";
            }
            echo "<xmp>";
            print_r($var);
            echo "</xmp>";
        }
    }
    
    function p($var, $url = false, $delimiter = false)
    {
        if (empty($url)) {
            $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
        }
        
        if (!empty($delimiter)) {
            $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
        } else {
            $delimiter = "\n\n-------------------------------------------------\n\n";
        }
        file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
    }
	
//функция заменяет id свойств на символьные коды 
function replaceIdWithCodeInSkuFilter($arOffersFilter)
{
	if(!empty($arOffersFilter))
	{
		\Bitrix\Main\Loader::includeModule("iblock");

		$arProperties = [];

		$properties = \Bitrix\Iblock\PropertyTable::getlist([
			'filter' => ['IBLOCK.CODE' => CATALOG_SKU_IBLOCK_CODE],
			'select' => ['ID', 'CODE'],
			'cache' => ['ttl' => 3600]
		]);

		while($property = $properties->fetch())
		{
			$arProperties[$property['ID']] = $property['CODE'];
		}
		
		foreach($arOffersFilter as $property => $value)
		{
			if(false !== strpos($property, 'PROPERTY'))
			{
				$newProperty = str_replace(array_keys($arProperties), $arProperties, $property);
				
				if($newProperty != $property)
				{
					$arOffersFilter[$newProperty] = $value;
					unset($arOffersFilter[$property]);
				}
			}
		}
	}
	
	return $arOffersFilter;
}

//функция возвращает таблицу с товарами из заказа для отправки в письме
function getOrderTable($basketItems){
    $totalSumm = 0;
    $orderTable = "<style>.order_table td, .order_table th{padding: 5px 10px;border-bottom: 1px solid #b5b5b5;}"
        . ".order_table th{font-weight: 600;background-color: #f0f8ff;}"
        . ".order_table .total td{font-weight: 600;}"
        . ".order_table .total>td:first-of-type{text-align: right;}"
        . ".order_table .centered{text-align: center;}"
        . "</style>";
    $orderTable .= "<table class='order_table'><tr><th>ID товара</th><th>Название</th><th>Размер</th><th>Количество</th><th>Стоимость</th><th>Сумма</th></tr>";
    foreach ($basketItems as $item) {
        $totalSumm += round($item['PRICE'] * $item['QUANTITY'], 2);
        $orderTable .= "<tr><td class='centered'><a href='".$item['DETAIL_PAGE_URL']."'>".$item['PRODUCT_ID'].
            "</a></td><td>".$item['NAME']." / ".$item['PRODUCT_XML_ID'].
            "</td><td class='centered'>".intval($item['SIZE']).
            "</td><td class='centered'>".intval($item['QUANTITY']).
            "</td><td class='centered'>".round($item['PRICE'], 2).
            "</td><td class='centered'>".round($item['PRICE'] * $item['QUANTITY'], 2)."</td></tr>";
    }
    $orderTable .= "<tr class='total'><td colspan=4>Итого:</td><td class='centered'>".$totalSumm."</td></tr></table>";
    return $orderTable;
}

function getOrderProperties($orderObject){
    $propertyCollection = $orderObject->getPropertyCollection();

    $propsData = [];
    foreach ($propertyCollection as $propertyItem) {
        if (!empty($propertyItem->getField("CODE"))) {
            $propsData[$propertyItem->getField("CODE")] = trim($propertyItem->getValue());
        }
    }
    return $propsData;
}

?>