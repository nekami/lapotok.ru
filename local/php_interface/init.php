<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

include_once(__DIR__ . "/include/functions.php");

include_once(__DIR__ . "/include/agents.php");

include_once(__DIR__ . "/include/const.php");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mcart.extramail/classes/general/include_part.php");

AddEventHandler('main', 'OnBeforeEventSend', 'OnBeforeEventSendHandler');

function OnBeforeEventSendHandler(&$arFields, &$arTemplate)
{
    // задание поля Кому
    $uf_default_to = \COption::GetOptionString("askaron.settings", "UF_DEFAULT_EMAIL_TO");

    if (!empty($uf_default_to)) {
        $arFields["DEFAULT_EMAIL_TO"] = $uf_default_to;
    }
}

AddEventHandler('main', 'OnBeforeEventAdd', 'OnBeforeEventAddHandler');

function OnBeforeEventAddHandler($event, $lid, &$arFields, &$message_id)
{
    if ($lid == "s1") {
        if ($event == "SALE_STATUS_CHANGED_P") {
            $message_id = 108;
            $order = \Bitrix\Sale\Order::load($arFields['ORDER_REAL_ID']);
            $orderProps = getOrderProperties($order);
            $arFields["USER_NAME"] = $orderProps['FIO'];
            $arFields["EMAIL"] = $orderProps['EMAIL'];
        } elseif ($event == "SALE_NEW_ORDER") {
            $message_id = 109;
            $order = \Bitrix\Sale\Order::load($arFields['ORDER_REAL_ID']);
            $orderProps = getOrderProperties($order);
            $arFields["USER_NAME"] = $orderProps['FIO'];
            $arFields["EMAIL"] = $orderProps['EMAIL'];
            $arFields["DELIVERY_ADDR"] = $orderProps['DELIVERY_ADDRESS'];
            $basket = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "LID" => $lid,
                    "ORDER_ID" => $arFields['ORDER_REAL_ID'],
                    "CAN_BUY" => "Y"
                ),
                false,
                false,
                array("ID", "PRICE", "DETAIL_PAGE_URL",
                    "PRODUCT_ID", "QUANTITY", "PRODUCT_XML_ID",
                    "NAME", "PRICE"
                )
            );
            $basketItemsIds = [];
            $basketItems = [];
            while ($item = $basket->Fetch()) {
                $basketItemsIds[] = $item['PRODUCT_ID'];
                $basketItems[$item['PRODUCT_ID']] = $item;
            }
            CModule::IncludeModule('iblock');
            $itemsSizeData = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("ID" => $basketItemsIds),
                false, false,
                array("ID", "PROPERTY_SIZE")
            );
            while ($itemSize = $itemsSizeData->Fetch()) {
                $basketItems[$itemSize['ID']]['SIZE'] = $itemSize['PROPERTY_SIZE_VALUE'];
            }
            $arFields['ORDER_TABLE'] = getOrderTable($basketItems);
        }
    }
}

//Фильтр возможности выбора пунктов выдачи в зависимости от наличия товара
AddEventHandler('sale', 'OnSaleComponentOrderOneStepDelivery', 'OnSaleComponentOrderOneStepDeliveryHandler');
function OnSaleComponentOrderOneStepDeliveryHandler(&$order)
{
    $productIds = [];
    $tempStores = [];
    foreach ($order['BASKET_ITEMS'] as $item) {
        $productIds[$item['PRODUCT_ID']] = $item['QUANTITY'];
    }
    if (!empty($productIds)) {
        $arSelect = array('ID', 'PRODUCT_AMOUNT', 'ELEMENT_ID');
        $rsStore = CCatalogStore::GetList(array(), array('PRODUCT_ID' => array_keys($productIds), '>PRODUCT_AMOUNT' => 0, 'ACTIVE' => 'Y'), false, false, $arSelect);
        while ($arStore = $rsStore->Fetch()) {
            if ($arStore['PRODUCT_AMOUNT'] >= $productIds[$arStore['ELEMENT_ID']]) {
                $tempStores[$arStore['ELEMENT_ID']][$arStore['ID']] = $arStore['ID'];
            }
        }
    }


    $activeStores = $tempStores[$order['BASKET_ITEMS'][0]['PRODUCT_ID']];
    $storeList = [];
    foreach ($tempStores as $store) {
        $activeStores = array_intersect($activeStores, $store);
    }

    \Bitrix\Main\Loader::includeModule("aspro.next");
    $currentRegionStores = CNextRegionality::getCurrentRegion()['LIST_STORES'];
    $activeStores = array_intersect($activeStores, $currentRegionStores);

    foreach ($order['DELIVERY'] as &$delivery) {
        foreach ($delivery['STORE'] as $key => $store) {
            if (!in_array($store, $activeStores)) {
                unset($delivery['STORE'][$key]);
            } else {
                $storeList[] = $store;
            }
        }
        $delivery['STORE'] = array_values($delivery['STORE']);
    }

    $resDel = \Bitrix\Sale\Delivery\Services\Table::getlist(['filter' => ['ID' => array_keys($order['DELIVERY']), 'PARENT.CODE' => 'DELIVERY_GROUPE']]);
    while ($del = $resDel->fetch()) {
        $order['DELIVERY'][$del['ID']]['DELIVERI_GROUPE'] = true;
    }
    $issetOneDelivery = false;
    $checkFirst = false;
    foreach ($order['DELIVERY'] as $id => &$delivery) {

        if (empty($delivery['STORE']) || $issetOneDelivery && $delivery['DELIVERI_GROUPE'] === true) {
            if ($delivery['CHECKED']) {
                $checkFirst = true;
            }
            unset ($order['DELIVERY'][$id]);
            //$delivery['LOW_AMMOUNT'] = true;
        } elseif ($delivery['DELIVERI_GROUPE'] === true) {
           $issetOneDelivery = true;
        }

    }
    if ($checkFirst) {
        //$order['DELIVERY'][array_keys($order['DELIVERY'])[0]]['CHECKED'] = "Y";
    }
    foreach ($order['STORE_LIST'] as $storeId => $store) {
        if (!in_array($storeId, $storeList)) {
            unset($order['STORE_LIST'][$storeId]);
        }
    }
}

//Отправка письма в выбранный или ближайший магазин при оформлении заказа
AddEventHandler('sale', 'OnOrderSave', 'OnOrderSaveHandler');
function OnOrderSaveHandler($orderId, $arFields, $arOrder, $isNew)
{
    if ($isNew) {
        $props = getPropsIds();
        $storeEmail = $storeName = $adress = '';
        $type = "Добавлена заявка на самовывоз";
        $defaultEmail = COption::GetOptionString("sale", "order_email", "");
        $basketItemsIds = [];
        foreach($arOrder['BASKET_ITEMS'] as $item){
            $basketItemsIds[] = $item['PRODUCT_ID'];
        }
        CModule::IncludeModule('iblock');
        $basketSizes = [];
        $itemsSizeData = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array("ID" => $basketItemsIds),
            false, false,
            array("ID", "PROPERTY_SIZE")
        );
        while ($itemSize = $itemsSizeData->Fetch()) {
            $basketSizes[$itemSize['ID']] = $itemSize['PROPERTY_SIZE_VALUE'];
        }
        foreach($arOrder['BASKET_ITEMS'] as &$item){
            $item['SIZE'] = $basketSizes[$item['PRODUCT_ID']];
        }

        $orderTable = getOrderTable($arOrder['BASKET_ITEMS']);

        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule('catalog');

        $delivery = \Bitrix\Sale\Delivery\Services\Manager::getById($arOrder['DELIVERY_ID']);
        if (intval($delivery['PARENT_ID']) != 0) {
            $parent = \Bitrix\Sale\Delivery\Services\Manager::getById($delivery['PARENT_ID']);
            if ($parent['CODE'] != "DELIVERY_GROUPE") {

                $dil = \Bitrix\Sale\Internals\ShipmentItemTable::GetList([
                    'filter' => ['BASKET.ORDER_ID' => intval($orderId), '!SHIPMENT.VALUE' => 0],
                    'select' => ['ID', 'SHIPMENT.VALUE', 'STORE.EMAIL', 'STORE.TITLE', 'STORE.ADDRESS'],
                    'runtime' => [
                        new \Bitrix\Main\ORM\Fields\Relations\Reference('SHIPMENT', \Bitrix\Sale\Internals\ShipmentExtraServiceTable::getEntity(), ['=this.ORDER_DELIVERY_ID' => 'ref.SHIPMENT_ID']),
                        new \Bitrix\Main\ORM\Fields\Relations\Reference('STORE', \Bitrix\Catalog\StoreTable::getEntity(), ['=this.SHIPMENT.VALUE' => 'ref.ID']),
                    ],
                ]);
                if ($shp = $dil->fetch()) {
                    $storeEmail = $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_EMAIL'];
                    $storeName = $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_TITLE'] . " (" . $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_ADDRESS'] . ")";
                }
            } else {
                $storeRes = \Bitrix\Catalog\StoreTable::GetList([
                    'filter' => ['ID' => $arOrder['ORDER_PROP'][$props['DELIVERY_STORE']]]
                ]);
                if ($store = $storeRes->fetch()) {
                    $storeEmail = $store['EMAIL'];
                    $storeName = $store['TITLE'] . " (" . $store['ADDRESS'] . ")";
                    $type = "Добавлена заявка на доставку";
                    $adress = "Адрес доставки: " . implode(", ", [
                            "г. " . $arOrder['ORDER_PROP'][$props['ADDRESS_CITY']],
                            "ул. " . $arOrder['ORDER_PROP'][$props['ADDRESS_STREET']],
                            "д. " . $arOrder['ORDER_PROP'][$props['ADDRESS_HOUSE']],
                        ]);
                }
            }
        } else {
            $dil = \Bitrix\Sale\Internals\ShipmentItemTable::GetList([
                'filter' => ['BASKET.ORDER_ID' => intval($orderId), '!SHIPMENT.VALUE' => 0],
                'select' => ['ID', 'SHIPMENT.VALUE', 'STORE.EMAIL', 'STORE.TITLE', 'STORE.ADDRESS'],
                'runtime' => [
                    new \Bitrix\Main\ORM\Fields\Relations\Reference('SHIPMENT', \Bitrix\Sale\Internals\ShipmentExtraServiceTable::getEntity(), ['=this.ORDER_DELIVERY_ID' => 'ref.SHIPMENT_ID']),
                    new \Bitrix\Main\ORM\Fields\Relations\Reference('STORE', \Bitrix\Catalog\StoreTable::getEntity(), ['=this.SHIPMENT.VALUE' => 'ref.ID']),
                ],
            ]);
            if ($shp = $dil->fetch()) {
                $storeEmail = $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_EMAIL'];
                $storeName = $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_TITLE'] . " (" . $shp['SALE_INTERNALS_SHIPMENT_ITEM_STORE_ADDRESS'] . ")";
            }
        }
        CEvent::Send(
            'SEND_EMAIL_TO_LOCAL_STORE',
            's1',
            [
                'TYPE' => $type,
                'ADDRESS' => $adress,
                'ORDER_ID' => $orderId,
                'DATE' => $arOrder['DATE_INSERT'],
                'ORDER_TABLE' => $orderTable,
                'USER_NAME' => $arOrder['ORDER_PROP'][$props['FIO']],
                'USER_PHONE' => $arOrder['ORDER_PROP'][$props['PHONE']],
                'SELECTED_STORE' => $storeName,
                'STORE_EMAIL' => [
                    $storeEmail,
                    //$defaultEmail
                ]
            ]
        );
    }
}

AddEventHandler('sale', 'OnSaleComponentOrderProperties', 'OnSaleComponentOrderPropertiesHandler');
function OnSaleComponentOrderPropertiesHandler(&$arUserResult, $request, &$arParams, &$arResult)
{
    $props = getPropsIds();
    foreach ($props as $propId => $prop) {
        $propVal = $request->get('ORDER_PROP_' . $prop);
        if ($propVal && empty($arUserResult['ORDER_PROP'][$prop])) {
            $arUserResult['ORDER_PROP'][$prop] = $propVal;
        }
        if ($propId === 'DELIVERY_ADDRESS') {
            $arUserResult['ORDER_PROP'][$props['DELIVERY_ADDRESS']] = $arUserResult['ORDER_PROP'][$props['ADDRESS']];
        }
    }
    $arUserResult['ORDER_PROP'][$props['DELIVERY_STORE']] = $arUserResult['BUYER_STORE'];
}

//Вычисление id свойств заказа
function getPropsIds()
{
    $props = [];
    $propRes = CSaleOrderProps::GetList([], ['PERSON_TYPE_ID' => 1], false, false, ['CODE', 'ID']);
    while ($prop = $propRes->fetch()) {
        $props[$prop['CODE']] = $prop['ID'];
    }
    return $props;
}

//обработчик события смены статуса заказа
AddEventHandler('sale', 'OnSaleStatusOrderChange', 'OnOrderStatusChangeHandler');
function OnOrderStatusChangeHandler($orderObject)
{
    /** @var Bitrix\Sale\Order $orderObject */
    $parameters = $orderObject->getFieldValues();
    if ($parameters['STATUS_ID'] === 'P') {
        $orderPropertiesCollection = $orderObject->getPropertyCollection();

        $needleProps = array(
            "DELIVERY_STORE",
            "DELIVERY_ADDRESS",
            "FIO",
            "PHONE",
        );
        $props = [];
        $orderProperties = $orderPropertiesCollection->getArray()['properties'];
        foreach ($orderProperties as $property) {
            if (in_array($property['CODE'], $needleProps)) {
                $props[$property['CODE']] = $property['VALUE'][0];
            }
        }
        $basket = CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "LID" => $parameters['LID'],
                "ORDER_ID" => $parameters['ID'],
                "CAN_BUY" => "Y"
            ),
            false,
            false,
            array("ID", "PRICE", "DETAIL_PAGE_URL",
                "PRODUCT_ID", "QUANTITY", "PRODUCT_XML_ID",
                "NAME", "PRICE"
            )
        );
        $basketItemsIds = [];
        $basketItems = [];
        while ($item = $basket->Fetch()) {
            $basketItemsIds[] = $item['PRODUCT_ID'];
            $basketItems[$item['PRODUCT_ID']] = $item;
        }
        CModule::IncludeModule('iblock');
        $itemsSizeData = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array("ID" => $basketItemsIds),
            false, false,
            array("ID", "PROPERTY_SIZE")
        );
        while ($itemSize = $itemsSizeData->Fetch()) {
            $basketItems[$itemSize['ID']]['SIZE'] = $itemSize['PROPERTY_SIZE_VALUE'];
        }

        $type = "Заказ оплачен, ожидает доставки №";
        $orderTable = getOrderTable($basketItems);

        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule('catalog');
        $delivery = \Bitrix\Sale\Delivery\Services\Manager::getById((int)$orderObject->getDeliverySystemId()[0]);
        if (intval($delivery['PARENT_ID']) != 0) {
            $parent = \Bitrix\Sale\Delivery\Services\Manager::getById($delivery['PARENT_ID']);
            if ($parent['CODE'] == "DELIVERY_GROUPE") {
                $storeRes = \Bitrix\Catalog\StoreTable::GetList([
                    'filter' => ['ID' => $props['DELIVERY_STORE']]
                ]);
                if ($store = $storeRes->fetch()) {
                    $storeEmail = $store['EMAIL'];
                    $storeName = $store['TITLE'] . " (" . $store['ADDRESS'] . ")";
                    $adress = "Адрес доставки: " . $props['DELIVERY_ADDRESS'];

                    CEvent::Send(
                        'SEND_EMAIL_TO_LOCAL_STORE',
                        's1',
                        [
                            'TYPE' => $type,
                            'ADDRESS' => $adress,
                            'ORDER_ID' => $orderObject->getId(),
                            'DATE' => ConvertTimeStamp($orderObject->getDateInsert()->getTimestamp(), "FULL"),
                            'ORDER_TABLE' => $orderTable,
                            'USER_NAME' => $props['FIO'],
                            'USER_PHONE' => $props['PHONE'],
                            'SELECTED_STORE' => $storeName,
                            'STORE_EMAIL' => [
                                $storeEmail,
                            ]
                        ]
                    );
                }
            }
        }
    }
}

AddEventHandler('sale', "OnSaleOrderBeforeSaved", "onBeforeOrderAddHandler");
function onBeforeOrderAddHandler($order)
{
    /** @var \Bitrix\Sale\Order $order */
    $propertyCollection = $order->getPropertyCollection();

    $propsData = [];
    foreach ($propertyCollection as $propertyItem) {
        if (!empty($propertyItem->getField("CODE"))) {
            $propsData[$propertyItem->getField("CODE")] = trim($propertyItem->getValue());
        }
    }

    foreach ($propertyCollection as $propertyItem) {
        if (in_array($propertyItem->getField("CODE"), ["ADDRESS", "DELIVERY_ADDRESS"])) {
            $val = implode(", ", [
                "г. " . $propsData['ADDRESS_CITY'],
                "ул. " . $propsData['ADDRESS_STREET'],
                "д. " . $propsData['ADDRESS_HOUSE']
            ]);
            $propertyItem->setField("VALUE", $val);
        }
    }
}

//включение возможности добавить товар в корзину в зависимости от настроек сайта
AddEventHandler('main', 'OnBeforeProlog', 'OnBeforePrologHandler');
function OnBeforePrologHandler()
{
    \Bitrix\Main\Loader::includeModule("aspro.next");

    global $APPLICATION, $arRegion, $arRegions;

    $arRegion = CNextRegionality::getCurrentRegion();

    if (!empty($arRegion['PROPERTY_REGION_TAG_CAN_BUY_IN_REGION_VALUE'])) {
        define("SHOW_ADD_TO_BASKET", true);
    } else {
        define("SHOW_ADD_TO_BASKET", false);
    }
}


//отправка пользователю сообщения после регистрации
AddEventHandler('main', 'OnAfterUserRegister', 'OnAfterUserRegisterHandler');
AddEventHandler('main', 'OnAfterUserSimpleRegister', 'OnAfterUserRegisterHandler');
AddEventHandler('main', 'OnAfterUserAdd', 'OnAfterUserRegisterHandler');

    function OnAfterUserRegisterHandler($arFields)
    {
        if ($arFields['USER_ID'] > 0 || $arFields['ID'] > 0) {
            CEvent::Send(
                'USER_REGISTER',
                's1',
                [
                    'USER_EMAIL' => $arFields['EMAIL'],
                    'USER_NAME' => $arFields['NAME'] . " " . $arFields['LAST_NAME'],
                    'USER_LOGIN' => $arFields['LOGIN'],
                    'USER_PASSWORD' => $arFields['PASSWORD'],
                ]
            );
        }
    }

?>