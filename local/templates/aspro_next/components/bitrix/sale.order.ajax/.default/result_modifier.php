<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$props = getPropsIds();
$arResult['ORDER_PROPS'] = $arResult['JS_DATA']['ORDER_PROPS'] = $props;
$arResult['JS_DATA']['MAX_DELIVERY_LENGHT'] = \COption::GetOptionString( "askaron.settings", "UF_MAX_DELIVERY_LENG");

$productIds = [];
$tempStores = [];
foreach ($arResult['BASKET_ITEMS'] as $item) {
	$productIds[$item['PRODUCT_ID']] = $item['QUANTITY'];
}
if (!empty($productIds)) {
	$arSelect = array('ID', 'PRODUCT_AMOUNT', 'ELEMENT_ID');
	$rsStore = CCatalogStore::GetList(array(), array('PRODUCT_ID' => array_keys($productIds), '>PRODUCT_AMOUNT' => 0, 'ACTIVE' => 'Y'), false, false, $arSelect);
	while($arStore = $rsStore->Fetch())
	{
		if ($arStore['PRODUCT_AMOUNT'] >= $productIds[$arStore['ELEMENT_ID']]) {
			$tempStores[$arStore['ELEMENT_ID']][$arStore['ID']] = $arStore['ID'];
		}
	}
}

$activeStores = $tempStores[$arResult['BASKET_ITEMS'][0]['PRODUCT_ID']];
foreach ($tempStores as $store) {
	$activeStores = array_intersect($activeStores, $store);
}

$storeList = [];
OnSaleComponentOrderOneStepDeliveryHandler($arResult);
foreach ($arResult['DELIVERY'] as $id => &$delivery) {
	foreach ($delivery['STORE'] as $key => $store) {
		if (!in_array($store, $activeStores)) {
			unset($delivery['STORE'][$key]);
		} else {
			$storeList[] = $store;
		}
    }
	$delivery['STORE'] = array_values($delivery['STORE']);
}
foreach ($arResult['JS_DATA']['DELIVERY'] as $id => &$delivery) {
    if(!in_array($id, array_keys($arResult['DELIVERY']))){
        unset($arResult['JS_DATA']['DELIVERY'][$id]);
        continue;
    }
	foreach ($delivery['STORE'] as $key => $store) {
		if (!in_array($store, $activeStores)) {
			unset($delivery['STORE'][$key]);
		}
	}
    $delivery['STORE'] = array_values($delivery['STORE']);
}
foreach ($arResult['JS_DATA']['STORE_LIST'] as $storeId => $store) {
	if (!in_array($storeId, $storeList)) {
		unset($arResult['JS_DATA']['STORE_LIST'][$storeId]);
	}
}
foreach ($arResult['STORE_LIST'] as $storeId => $store) {
	if (!in_array($storeId, $storeList)) {
		unset($arResult['STORE_LIST'][$storeId]);
	}
}

/*$curLoc = [];
foreach ($arResult['ORDER_PROP']['USER_PROPS_N'][$props['LOCATION']]['VARIANTS'] as $key => $loc) {
	if ($loc['CODE'] === $arResult['USER_VALS']['DELIVERY_LOCATION_BCODE']) {
		$curLoc[$key] = $loc;
	}
}
$arResult['ORDER_PROP']['USER_PROPS_N'][$props['LOCATION']]['VARIANTS'] = $curLoc;
$arResult['ORDER_PROP']['USER_PROPS_N'][$props['LOCATION']]['~VARIANTS'] = $curLoc;
echo "<pre>".print_r($arResult, true)."</pre>";*/
$obPickupId = Bitrix\Sale\Delivery\Services\Table::GetList(
[
	'filter' => ['ID' => array_column($arResult['JS_DATA']['DELIVERY'], 'ID'),
				 'PARENT_ID' => 101],
	'select' => ['ID']
]);

while ($arPickupId = $obPickupId->fetch())
{
	$arResult['PICKUP_ID'][] = $arPickupId["ID"];
}

$component = $this->__component;
$component->SetResultCacheKeys(array('PICKUP_ID', 'ORDER_PROPS', 'MAX_DELIVERY_LENGTH'));

foreach ($arResult['JS_DATA']['DELIVERY'] as $Delivery)
{
	if ( ($Delivery['CHECKED'] == "Y") && in_array($Delivery['ID'], $arResult['PICKUP_ID']) )
	{	
		foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $Prop)
		{
			if ($Prop["CODE"] == "ADDRESS")
			{
				unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]);
			}
		}
	}		
}

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */


$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);
\Bitrix\Main\Loader::includeModule("aspro.next");

$arResult['REGION'] = CNextRegionality::getCurrentRegion();