<?
$company = \Bitrix\Sale\CompanyTable::getList(
    array(
        "select" => array(
            "ID",
            "NAME",
            "UF_INN",
            "UF_OGRN",
            "UF_RS",
            "UF_CS",
            "UF_BANK",
            "UF_BIK",
        ),
        "filter" => array(
            "UF_SHOP" => $arResult['ID'],
        )
    )
)->fetch();
/*
 * *array(7) {
  ["ID"]=>
  string(1) "3"
  ["UF_INN"]=>
  string(12) "525603021768"
  ["UF_OGRN"]=>
  string(46) "318527500050351  от 18 апреля 2018 г."
  ["UF_RS"]=>
  string(20) "40802810242000020237"
  ["UF_CS"]=>
  string(20) "30101810900000000603"
  ["UF_BANK"]=>
  string(58) "Волго-Вятский банк ПАО Сбербанк"
  ["UF_BIK"]=>
  string(9) "042202603"
}
 */
//$shop = CCatalogStore::GetList();
$arResult['COMPANY'] = $company;
$this->__component->arResultCacheKeys = array_merge($this->__component->arResultCacheKeys, array('ID', 'IBLOCK_SECTION_ID', 'DISPLAY_PROPERTIES'));

?>